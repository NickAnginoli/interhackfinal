const DadosCtrl = (function () {
    
    const Questao = function (pergunta, resposta) {
        this.pergunta = pergunta;
        this.resposta = resposta;
    }
    
    dados = {
        perguntaAtual: 0,
        correta: false,
        questoes: [
            new Questao('Escreva o que você ouviu', 'amo cozinhar'),
            new Questao ('Qual dessas figuras é um bolo ?', 'card-0'),
            new Questao('Leia essa frase', 'eu estou aprendendo')
        ]
    };

    return {
        pegarPerguntaAtual: function () {
            return dados.perguntaAtual;
        },

        atualizaPerguntaAtual: function (atualizada) {
            dados.perguntaAtual = atualizada;
        },

        pegarDados: function () {
            return dados;
        }
    }

})();

const AudioCtrl = (function () {
    
    window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition
    
    const reconhecedorDeVoz = new window.SpeechRecognition();

    reconhecedorDeVoz.continuous = true;

    const estado = {
        ouvindo: false,
        terminou: true,
        transcricao: ''
    }

    reconhecedorDeVoz.onresult = function(event){        
        for (var i = event.resultIndex; i < event.results.length; i++) {
            if(event.results[i].isFinal){
                estado.transcricao = event.results[i][0].transcript;
            } else{
                estado.transcricao += event.results[i][0].transcript;
            }
        }
    }
    
    return {
        ouvirResposta: function() {
            estado.transcricao = '';           
            
            try {
                reconhecedorDeVoz.start();
                estado.ouvindo = true;
                estado.terminou = false; 

            } catch(ex) {
                alert("error: "+ ex.message);
            }
        }, 

        pararDeOuvir: function() {
            reconhecedorDeVoz.stop();

            estado.ouvindo = false;
            estado.terminou = true;
        },

        pegarResposta: function () {
            return estado.transcricao;
        },

        pegarEstado: function () {
            return estado;
        }
    }
})();

const UICtrl = (function () {
    
let pergunta0 = `<div class="titulo">
<button class="btn-audio-sm btn-escreva"> <i class="fa fa-volume-up" aria-hidden="true"></i> </button>
<audio src="escreva-ouviu.mpeg" class="audio-escreva"></audio>
<h3>Escreva o que você ouviu</h3>
</div>

<div class="conteudo">

<div class="">
    <button class="btn-audio-lg btn-cozinhar"> <i class="fa fa-volume-up" aria-hidden="true"></i> </button>
    <audio src="amo-cozinhar.mpeg" class="audio-cozinhar"></audio>
</div>

<div class="resposta">
    <input type="text" class="resposta-input">
</div>
</div>

<div class="prox-atividade">
<button class="btn-prox"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
</div>
`;

let pergunta1 = `
<div class="titulo">
<button class="btn-audio-sm btn-qual-bolo"> <i class="fa fa-volume-up" aria-hidden="true"></i> </button>
<audio src="qual-bolo.mpeg" class="audio-qual-bolo"></audio>
<h3>Qual dessas figuras é um bolo ?</h3>
</div>

<div class="conteudo">

<div class="card" id="card-0">
    <img src="bolo.png" alt="">
    <div class="card-descricao">
        <div class="card-descricao-titulo">Bolo</div>
        <div class="opcao">1</div>
    </div>
</div>

<div class="card" id="card-1">
    <img src="sorvete.png" alt="">
    <div class="card-descricao">
        <div class="card-descricao-titulo">Sorvete</div>
        <div class="opcao">2</div>
    </div>
</div>

<div class="card" id="card-2">
    <img src="suco.png" alt="">
    <div class="card-descricao">
        <div class="card-descricao-titulo">Suco</div>
        <div class="opcao">3</div>
    </div>
</div>
</div>

<div class="prox-atividade">
<button class="btn-prox"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
</div>
`;

let pergunta2 = `<div class="titulo">
<button class="btn-audio-sm btn-leia"> <i class="fa fa-volume-up" aria-hidden="true"></i> </button>
<audio src="leia.mpeg" class="audio-leia"></audio>
<h3>Leia esta frase</h3>
</div>

<div class="conteudo">

<div >
    <button class="btn-microfone"> <i class="fa fa-microphone" aria-hidden="true"></i> </button>
</div>

<div class="frase">
    <button class="btn-audio-sm"> <i class="fa fa-volume-up" aria-hidden="true"></i> </button>
    <span> Eu estou aprendendo !</span>
</div>

</div>

<div class="prox-atividade">
<button class="btn-prox"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
</div>`;

let finalizada = `
<div class="titulo tarefa-finalizada">
    <div class="tarefa-finalizada-img"> 
        <img src="festa.png" alt="">
    </div>

    <div class="tarefa-finalizada-txt"> 
        <h3>Tarefa Finalizada !</h3>
        <p> Parabéns pela evolução </p>
        <button class="btn-retornar"><i class="fa fa-home" aria-hidden="true"></i> Voltar</button>
    </div>
    
</div>
`
    
    
const questoesUI = [pergunta0, pergunta1, pergunta2];
    
    return {
        toggleMicrofone: function (ouvindo) {
            if(ouvindo) document.querySelector('.btn-microfone').parentElement.innerHTML = `
                <button class="btn-microfone"> <i class="fa fa-square" aria-hidden="true"></i> </button>
            `;

            else document.querySelector('.btn-microfone').parentElement.innerHTML = `<button class="btn-microfone"> <i class="fa fa-microphone" aria-hidden="true"></i> </button>`
        },

        exibeProximaQuestao: function (proxima) {
            document.querySelector('.atividade').innerHTML = questoesUI[proxima];
            document.querySelector('.barra').style.width = `${(proxima * (100/questoesUI.length)/100) * 100}%`;
        },

        pegarResposta: function () {
            return document.querySelector('.resposta-input').value;
        },

        finalizarAula: function (proxima) {
            document.querySelector('.atividade').innerHTML = finalizada;
            document.querySelector('.barra').style.width = `${(proxima * (100/questoesUI.length)/100) * 100}%`;  
        }, 

        selecionaCard: function (id) {
            document.querySelectorAll('.card').forEach((el) => {
                el.classList.remove('selecionado');
            })
            
            const card = document.getElementById(`${id}`);
            card.classList.toggle('selecionado');
        },

        exibeCorrecao: function (correta) {
            if(correta) {
                document.querySelector('.correcao').innerHTML = `
                <div class="container">
                    <h3>Resposta Correta</h3>
                    <h4>Parabéns !</h4>
                </div>
                `;
                document.querySelector('.correcao').style.backgroundColor = 'rgba(0, 128, 0, 0.35)';
            }

            else {
                document.querySelector('.correcao').innerHTML = `
                    <div class="container">
                        <h3>Resposta Incorreta</h3>
                        <h4>Tente Novamente</h4>
                    </div>
                `
                document.querySelector('.correcao').style.backgroundColor = 'rgba(255, 0, 0, 0.35)';
            }
        },

        resetaCorrecao: function() {
            document.querySelector('.correcao').innerHTML = ``;
            document.querySelector('.correcao').style.backgroundColor = '#fff';
        }
    }
})();

const AppCtrl = (function () {
    
    const proximaQuestao = function () {
        const perguntaAtual = DadosCtrl.pegarPerguntaAtual();

        

        if(perguntaAtual === 1) {
            const resposta = document.querySelector('.selecionado').id;
            checaResposta(resposta);
        }

        else if(perguntaAtual === 2) {
            const resposta = AudioCtrl.pegarResposta();
            checaResposta(resposta);
        }

        else if (perguntaAtual === 0) {
            const resposta = UICtrl.pegarResposta();
            checaResposta(resposta);
        }
        
        if(DadosCtrl.pegarDados().correta) {
            
            DadosCtrl.atualizaPerguntaAtual(DadosCtrl.pegarPerguntaAtual() + 1);
            
            if(DadosCtrl.pegarPerguntaAtual() == 3) {
                setTimeout(() => {
                    UICtrl.resetaCorrecao();
                    UICtrl.finalizarAula(3);
                }, 2000)   
                
                return;
            }
            
            setTimeout(() => {
                UICtrl.exibeProximaQuestao(DadosCtrl.pegarPerguntaAtual());
                UICtrl.resetaCorrecao();
                DadosCtrl.pegarDados().correta = false;
            }, 2000)       
            
        }
    }

    const checaResposta = function (resposta) {
        const dados = DadosCtrl.pegarDados();
        
        const questoes = dados.questoes;
        const perguntaAtual = dados.perguntaAtual;
        let respostaAtual = resposta;

        if(respostaAtual == 'Amo cozinhar') respostaAtual = 'amo cozinhar';

        if(questoes[perguntaAtual].resposta == respostaAtual) {
            dados.correta = true;
            UICtrl.exibeCorrecao(dados.correta);
        }
        else {
            dados.correta = false;
            UICtrl.exibeCorrecao(dados.correta);
            setTimeout(() => {
                UICtrl.resetaCorrecao();
            }, 2000)
        }

    }
    
    const setEventListeners = function () {
        document.querySelector('.atividade').addEventListener('click', (e) => {
            
            console.log(e.target);
            
            if(e.target.parentNode.classList.contains('btn-prox')) {
                proximaQuestao();
            }

            else if(e.target.parentNode.classList.contains('btn-microfone') || e.target.classList.contains('btn-microfone')) {
                
                if(!AudioCtrl.pegarEstado().ouvindo && AudioCtrl.pegarEstado().terminou) {
                    AudioCtrl.ouvirResposta();
                    UICtrl.toggleMicrofone(AudioCtrl.pegarEstado().ouvindo);
                }
                
                else {
                    setTimeout(() => {
                        AudioCtrl.pararDeOuvir();
                        UICtrl.toggleMicrofone(AudioCtrl.pegarEstado().ouvindo);
                    }, 1000);
                }
            }

            else if(e.target.parentNode.classList.contains('card') || e.target.classList.contains('card'))  {
                
                let id;
                
                if(e.target.parentNode.classList.contains('card')){
                    id = e.target.parentNode.id;
                }

                else id = e.target.id;

                UICtrl.selecionaCard(id);
            }

            else if(e.target.parentNode.classList.contains('btn-leia') || e.target.classList.contains('btn-leia')) {
                document.querySelector('.audio-leia').play();
            }

            else if(e.target.parentNode.classList.contains('btn-escreva') || e.target.classList.contains('btn-escreva')) {
                document.querySelector('.audio-escreva').play();
            }

            else if(e.target.parentNode.classList.contains('btn-cozinhar') || e.target.classList.contains('btn-cozinhar')) {
                document.querySelector('.audio-cozinhar').play();
            }

            else if(e.target.parentNode.classList.contains('btn-qual-bolo') || e.target.classList.contains('btn-qual-bolo')) {
                document.querySelector('.audio-qual-bolo').play();
            }

        })

    }


    return {
        init: function () {
            setEventListeners();
            UICtrl.exibeProximaQuestao(DadosCtrl.pegarPerguntaAtual());
        }
    }
})(DadosCtrl, AudioCtrl, UICtrl);

AppCtrl.init();